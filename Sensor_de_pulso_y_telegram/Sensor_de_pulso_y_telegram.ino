
#include <Wire.h>
#include "MAX30105.h"           //MAX3010x library
#include "heartRate.h"          //Heart rate calculating algorithm
#include "spo2_algorithm.h"

#ifdef ESP32
  #include <WiFi.h>
#else
  #include <ESP8266WiFi.h>
#endif
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>   // Universal Telegram Bot Library written by Brian Lough: https://github.com/witnessmenow/Universal-Arduino-Telegram-Bot
#include <ArduinoJson.h>

///

MAX30105 particleSensor;

const byte RATE_SIZE = 4; //Increase this for more averaging. 4 is good.
byte rates[RATE_SIZE]; //Array of heart rates
byte rateSpot = 0;
long lastBeat = 0; //Time at which the last beat occurred
float beatsPerMinute;
int beatAvg;



////////////////////////////////////WIFI///////////////////////////////////////

// Replace with your network credentials
const char* ssid = "Meganet_Familia_Gudiel_Yanes";
const char* password = "0rd3n@d0r";

// Initialize Telegram BOT
#define BOTtoken "1281066977:AAH_1z3VjukbGuX4vEIQ46KFYHeIKvQVce0"  // your Bot Token (Get from Botfather)

// Use @myidbot to find out the chat ID of an individual or a group
// Also note that you need to click "start" on a bot before it can
// message you
#define CHAT_ID "141667573"

WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

 ////////////////////////////////////////////////////////////////////////////                      
                                    
int num = 0;
int contador = 0;
int tiempo = 0;
boolean stoptimer = false;

void setup() {  
  
  Serial.begin(9600);

  #ifdef ESP8266
    client.setInsecure();
  #endif
  
  // Connect to Wi-Fi
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());
  
  
  // Initialize sensor

 
 Serial.println("Initializing...");

  // Initialize sensor
  if (!particleSensor.begin(Wire, I2C_SPEED_FAST)) //Use default I2C port, 400kHz speed
  {
    Serial.println("MAX30105 was not found. Please check wiring/power. ");
    while (1);
  }
  Serial.println("Place your index finger on the sensor with steady pressure.");

  particleSensor.setup(); //Configure sensor with default settings
  particleSensor.setPulseAmplitudeRed(0x0A); //Turn Red LED to low to indicate sensor is running
  particleSensor.setPulseAmplitudeGreen(0); //Turn off Green LED
  
  //PARA COMPROBAR SI ESTA CONECTADO A LA RED WIFI Y QUE ENVIE UN SMS A TELEGRAM QUE EL DISPOSITIVO ESTA EN LINEA
  if(WiFi.status() == WL_CONNECTED)
  {
  bot.sendMessage(CHAT_ID, "DISPOSITIVO EN LINEA. Ya puedes comenzar hacer tus Test. :).\n\n BPM(Latidos Por Minuto) \n <(Menor) \n >(Mayor) \n\n *Estados del Pulso Cardiaco* \n\n -Bradicardia \n BPM<60. \n -Pulso Cardiaco en Reposo \n BPM>60 Y BPM<90.\n -Taquicardia \n BPM>90.\n\n  ¡Frecuencia cardíaca normal y máxima según la edad! \n\n Niños 100 - 115 BPM \n Adultos 60 - 80 BPM. \n Anciano 50 - 60 BPM.", "");
  } 
   

}

void loop() {

  //////////////////////////////PARA EL TIMER///////////////////////////////////////
  contador++;
    // Serial.println(tiempo);

     if(contador ==30)
     {
      tiempo++;
       Serial.println(tiempo);
      contador =0;
     }
     if(tiempo == 61)
     {
      stoptimer = true;
      
      }

  
  
 long irValue = particleSensor.getIR();    //Reading the IR value it will permit us to know if there's a finger on the sensor or not
 //Serial.println(irValue);                                         //Also detecting a heartbeat
if(irValue > 7000 && stoptimer == false){                                           //If a finger is detected
  
Serial.print("bpm1.val=");
Serial.print(beatAvg);
Serial.write(0xfff);
Serial.write(0xfff);
Serial.write(0xfff);  

Serial.print("timer1.val=");
Serial.print(60 - tiempo);
Serial.write(0xfff);
Serial.write(0xfff);
Serial.write(0xfff);  

int Value = map(analogRead(irValue),0,1024,0,255);  //Read the pot value ann map it to 0.255 (max value of waveform=255)
  String Tosend = "add ";                                       //We send the string "add "
  Tosend += 2;                                                  //send the id of the block you want to add the value to
  Tosend += ",";  
  Tosend += 0;                                                  //Channel of taht id, in this case channel 0 of the waveform
  Tosend += ",";
  Tosend += Value;                                              //Send the value and 3 full bytes
  Serial.print(Tosend);
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
  delay(50);
      
  
    
  if (checkForBeat(irValue) == true && stoptimer == false)                        //If a heart beat is detected
  {

   // Serial.print(60 - tiempo);
    long delta = millis() - lastBeat;                   //Measure duration between two beats
    lastBeat = millis();

    beatsPerMinute = 60 / (delta / 1000.0);           //Calculating the BPM

    if (beatsPerMinute < 255 && beatsPerMinute > 20)    // Para calcular el promedio, almacenamos algunos valores (4) y luego hacemos algunos cálculos para calcular el promedio           
    {
      rates[rateSpot++] = (byte)beatsPerMinute; //Store this reading in the array
      rateSpot %= RATE_SIZE; //Wrap variable

      //Take average of readings
      beatAvg = 0;
      for (byte x = 0 ; x < RATE_SIZE ; x++)
        beatAvg += rates[x];
      beatAvg /= RATE_SIZE;
    }
  }


}
  if (irValue < 5000){       //If no finger is detected it inform the user and put the average BPM to 0 or it will be stored for the next measure
     beatAvg=0;
      
     contador =0;
     tiempo = 0;
     beatAvg = 0;
     stoptimer = false;
     Serial.print("bpm1.val=");
     Serial.print(beatAvg);
     Serial.write(0xfff);
     Serial.write(0xfff);
     Serial.write(0xfff);  

      Serial.print("timer1.val=");
     Serial.print(tiempo);
     Serial.write(0xfff);
     Serial.write(0xfff);
     Serial.write(0xfff);  

  int Value = map(analogRead(beatAvg),0,1024,0,255);  //Read the pot value ann map it to 0.255 (max value of waveform=255)
  String Tosend = "add ";                                       //We send the string "add "
  Tosend += 2;                                                  //send the id of the block you want to add the value to
  Tosend += ",";  
  Tosend += 0;                                                  //Channel of taht id, in this case channel 0 of the waveform
  Tosend += ",";
  Tosend += Value;                                              //Send the value and 3 full bytes
  Serial.print(Tosend);
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
  delay(50);

     }
     
    if(stoptimer == true)
    {

        Serial.print("page pagina3");          
        Serial.write(0xfff);
        Serial.write(0xfff);
        Serial.write(0xfff); 
         Serial.print("page pagina3");          
        Serial.write(0xfff);
        Serial.write(0xfff);
        Serial.write(0xfff); 
        Serial.print("page pagina3");          
        Serial.write(0xfff);
        Serial.write(0xfff);
        Serial.write(0xfff); 
        Serial.print("page pagina3");          
        Serial.write(0xfff);
        Serial.write(0xfff);
        Serial.write(0xfff); 
       
       String sms;
       sms = (String)"TEST TERMINADO \n\n Su Resultado es \n\n Latidos por Minuto: " + beatAvg + (String)" \n\nNOTA. Compara tu test con los datos proporcionados anteriormente si no te encuentras dentro de lo normal debes de acudir a un medico.";
       bot.sendMessage(CHAT_ID, sms, "");
     contador =0;
     tiempo= 0;

      delay(1000);

     stoptimer = false;
      
    }

}
